#AskRideServer#

This project is using [Play Framework](https://www.playframework.com/https://www.playframework.com/) and [SecureSocial](http://securesocial.ws) to authenticate with Facebook only. In later version we will add authentication through Google and Twitter too.

Versions used:
* Scala 2.11
* Play 2.3.8
* SecureSocial 2.11-master-SNAPSHOT

SecureSocial's assets from [securesocial_2.11-3.0-M3-assets.jar](http://search.maven.org/remotecontent?filepath=ws/securesocial/securesocial_2.11/3.0-M3/securesocial_2.11-3.0-M3-assets.jar) have been extracted to *public/* because there seems to be an [issue with resource packaging with Play 2.3 and SecureSocial 3.0](https://groups.google.com/d/msg/securesocial/_1umOEL_uNA/aA9uk8tTW0cJ)

##How to Run the project##

1. Download Play 2.3.8, extract and add activator path to the PATH environment variable.
2. Check out project from bitbucket repository.
3. Go to AskRideServer directory from command prompt/terminal.
4. Then execute activator command and it will eventually go to $ prompt. Then execute run command to start the project. OR - Run ```activator run``` from the project directory to start the project.
5. once the project starts you need to receive accessToken and expiresIn from Facebook(from mobile platform) to get next level of token from SecureSocial server for successive service call.
6. once you receive the accessToken and expiresIn from FB, you can save it as test.json file in following JSON format -
    
	JSON Sample file saved as test.json: 
	-----------------------------------
	{
	   "email": "palakdebnath@gmail.com",
	   "info": {
		   "accessToken": "CAAMxDp5lKYgBAOIEFWFsPtEdYc6v0P69ut7Qe8JL5YKuzwqSHxPa1kTY6ZAqhY3n3pXkvpZACDvXuHL6zSoZA2DYpufw85XhRqVTYAReIGUry6WmwQIQuEY9zDYXaplgIHg05pZAGSVKFnbMVvrQV2CSRF6J2Ung5ZB0XyQNKmXyZCVnpagoGUgRGyar1n14XKxvjh0m3PrtbZB4yCUaZBRz",
	   "expiresIn": 5183998
	   }
    }

	and call following URI to get the TOKEN from server -
	
	curl -v --header "Content-Type: application/json" --request POST --data-binary "@test.json"  http://localhost:9000/auth/api/authenticate/facebook

	    
7. Once you get the new TOKEN (this TOKEN is different from the accessToken sent by FB) from sever you can use it to call any secure service running on the server. For example- /userExist is a route to access secure method userExist(). You can invoke userExist() in following way-

    curl -v --header "Content-Type: application/json" -H "X-Auth-Token: a5b6efc5fad6fafbd17803f57ad53d2c62fe07e49fc58e98c9b13a985de87d3a84aeb83d3e38151a173f245bf36d94485beaeda565eb5ac25126a6ccc24754f877a5f62aceff510a8f87390309bb6e6597643f214774219b7ae3b3be807ec5df118c40242b9630b99f52d4e9bc3f2c2d222fc2f7459932325e38f0d493542018" http://localhost:9000/userExist


