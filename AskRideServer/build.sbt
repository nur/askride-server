name := "AskRideServer"

version := "1.0-SNAPSHOT"

resolvers += Resolver.sonatypeRepo("snapshots")

lazy val root = (project in file(".")).enablePlugins(PlayJava).enablePlugins(SbtWeb)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
    javaCore,
    javaJdbc,
    javaEbean,
    cache,
    "org.postgresql" % "postgresql" % "9.2-1003-jdbc4",	
    "ws.securesocial" %% "securesocial" % "master-SNAPSHOT",
	"com.stripe" % "stripe-java" % "1.24.1",
    "commons-io" % "commons-io" % "2.3",
    "org.webjars" % "bootstrap" % "3.2.0"
)

// for less when it will work
includeFilter in (Assets, LessKeys.less) := "*.less"

excludeFilter in (Assets, LessKeys.less) := "_*.less"
