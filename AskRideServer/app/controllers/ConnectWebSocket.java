package controllers;


import manager.ChatRoom;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.libs.Json;
import play.mvc.Controller;
import play.mvc.WebSocket;



public class ConnectWebSocket extends Controller {
	
	private static final int SUCCESS=0;
	private static final int FAILURE=1;
	private static final char MILE='M';
	private static final double RANGE_LIMIT=5.0;
    private static final char ACTIVE='Y';
    private static final char INACTIVE='N';
    private static final char PASSENGER='P';
    private static final char DRIVER='D';
	
	//static Set<Session> chatroomUsers =  Collections.synchronizedSet(new HashSet<Session>());

    
    /**
public static WebSocket<String> socket(String userName) {
	
    return new WebSocket<String>() {

        // Called when the Websocket Handshake is done.
        public void onReady(WebSocket.In<String> in, WebSocket.Out<String> out) {

            // For each event received on the socket,
            in.onMessage(new Callback<String>() {
                public void invoke(String event) {

                    // Log events to the console
                    System.out.println("PALAK ==> " + event);

                }
            });

            // When the socket is closed.
            in.onClose(new Callback0() {
                public void invoke() {

                    System.out.println("Disconnected");

                }
            });
            
            
            // Send a single 'Hello!' message
            out.write("Hello " + "Palak" + "!");

        }

    };
}
**/


/**
 * Handle the chat websocket.
 */
public static WebSocket<JsonNode> chat(final String username) {
    return new WebSocket<JsonNode>() {
        
        // Called when the Websocket Handshake is done.
        public void onReady(WebSocket.In<JsonNode> in, WebSocket.Out<JsonNode> out) {
            
        	// boolean exists = false;
    		ObjectNode result = Json.newObject();
    		
            // Join the chat room.
            try { 
                ChatRoom.join(username, in, out);
                
                // Send a join confirmation to driver
                result.put("status",SUCCESS);
                result.put("message",username + " - Successfully joined to Server"); //0-> Success  
                // out.write("Hello " + "Palak" + "!");
                
            } catch (Exception ex) {
            	result.put("status",FAILURE); //0-> Success    1->Failure
    			result.put("message",username + " - Error joining to Server"); //0-> Success    1->Failure
                ex.printStackTrace();
            }
            out.write(result);
        }
    };
}
}
