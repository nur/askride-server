package controllers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import models.Car;
import models.Driver;
import models.User;
import models.UserLocation;

import com.fasterxml.jackson.databind.node.ObjectNode;

import play.libs.Json;
import play.mvc.Result;

import com.fasterxml.jackson.databind.JsonNode;

import play.mvc.Controller;

public class DriverController extends Controller{

	private static final int SUCCESS=0;
	private static final int FAILURE=1;
    /*
     * Example POST request ->
     * 
     * curl -X POST -H "Content-Type: application/json" -d " { \"user_name\":\"686023461494046\", \"first_name\":\"Palak\", \"last_name\":\"Debnath\", \"phone_no\":\"2244001301\", \"dl_no\":\"D1531210\", \"dl_state\":\"IL\", \"dl_issue_date\":\"02-SEP-11\", \"dl_expiration_date\":\"02-SEP-16\"}" http://localhost:9000/saveDriverInfo
     * 
     * curl -X POST -H "Content-Type: application/json" -d " { \"userName\":\"686023461494046\", \"firstName\":\"Palak\", \"lastName\":\"Debnath\", \"phoneNumber\":\"2244001301\", \"drivingLicenseNumber\":\"D1531210\", \"drivingLicenseState\":\"IL\", \"drivingLicenseIssueDate\":\"02-SEP-11\", \"drivingLicenseExpirationDate\":\"02-SEP-16\", \"carPlateNumber\":\"R7857Y\", \"carMake\":\"Toyota\", \"carModel\":\"Corolla\", \"carYear\":\"2006\", \"carColor\":\"Gray\"}" http://localhost:9000/saveDriverCarInfo
     * 
     */
	public static Result saveDriverCarInfo() {
		
		JsonNode val = request().body().asJson();
		ObjectNode result = Json.newObject();

		// System.out.println(val);
		String userName=val.findPath("userName").textValue();
		String firstName = val.findPath("firstName").textValue();
		String lastName = val.findPath("lastName").textValue();
		String phoneNumber   = val.findPath("phoneNumber").textValue();
				
		String drivingLicenseNumber =val.findPath("drivingLicenseNumber").textValue();
		String drivingLicenseState=val.findPath("drivingLicenseState").textValue();
		String drivingLicenseIssueDate=val.findPath("drivingLicenseIssueDate").textValue();
		String drivingLicenseExpirationDate=val.findPath("drivingLicenseExpirationDate").textValue();

		System.out.println("user_name = " + userName);
		System.out.println("dl_issue_date = " + drivingLicenseIssueDate);
		System.out.println("dl_expiration_date = " + drivingLicenseExpirationDate);
		
        //Update User details first
		User user = User.usersByUserName(userName);
		user.first_name = firstName;
		user.last_name = lastName;
		user.phoneno = phoneNumber;
		user.save();
		
		System.out.println("User Info Saved!");

		//Save Driver details now
		Driver driver = new Driver();
		driver.user_name = userName;
	    driver.dl_no = drivingLicenseNumber;
	    driver.dl_state = drivingLicenseState;
	    
	    DateFormat format = new SimpleDateFormat("DD-MM-YY");
	    Date dl_issue_dt = null;
	    Date dl_expiration_dt = null;
	    
	    try {
			dl_issue_dt = format.parse(drivingLicenseIssueDate);
		    dl_expiration_dt = format.parse(drivingLicenseExpirationDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		driver.dl_issue_date = dl_issue_dt;
	    driver.dl_expiration_date = dl_expiration_dt;
	    
	    driver.save();
		
		System.out.println("Driver data saved...");
		
		String carPlateNumber =val.findPath("carPlateNumber").textValue();
		String carMake=val.findPath("carMake").textValue();
		String carModel=val.findPath("carModel").textValue();
		int carYear=val.findPath("carYear").asInt();
		String carColor=val.findPath("carColor").textValue();

		//Save Car details
		Car car = new Car();
		car.registration_no = carPlateNumber;
		car.make = carMake;
		car.model = carModel;
		car.year = carYear;
		car.color = carColor;
		car.save();

		System.out.println("Car data saved...");
		
		result.put("status",SUCCESS); //0-> Success    1->Failure
		result.put("message","Driver info Added Successfully"); 

		return ok(result);
	}
	
	public static Result makeDriverAvailable(String userName) {

	    JsonNode val = request().body().asJson();
	    ObjectNode result = Json.newObject();

	    UserLocation userLoc = UserLocation.findAllUserLocationByUserName(userName);  //.findByID(userName)
	    userLoc.active_ind = 'Y';
	    userLoc.save();

	    result.put("status",SUCCESS); //0-> Success    1->Failure
	    result.put("message","Driver made available Successfully");
	    return ok(result);
	}

	public static Result makeDriverNotAvailable(String userName) {

	    JsonNode val = request().body().asJson();
	    ObjectNode result = Json.newObject();

	    UserLocation userLoc = UserLocation.findAllUserLocationByUserName(userName);
	          userLoc.active_ind = 'N';
	    userLoc.save();

	    result.put("status",SUCCESS); //0-> Success    1->Failure
	    result.put("message","Driver made NOT available Successfully");
	    return ok(result);
	}

}

