package controllers;

import models.Car;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.libs.Json;
import play.mvc.Result;

import com.fasterxml.jackson.databind.JsonNode;

import play.mvc.Controller;

public class CarController extends Controller{

	private static final int SUCCESS=0;
	private static final int FAILURE=1;
    /*
     * Example POST request ->
     * 
     *   curl -X POST -H "Content-Type: application/json" -d " {\"driver_id\":\"1007\", \"car_reg_no\":\"R7857Y\", \"car_make\":\"Toyota\", \"car_model\":\"Corolla\", \"car_year\":\"2006\", \"car_color\":\"Gray\"}" http://localhost:9000/saveCarInfo
     * 
     */
	
	public static Result saveCarInfo() {

		JsonNode val = request().body().asJson();
		ObjectNode result = Json.newObject();

		String car_reg_no =val.findPath("car_reg_no").textValue();
		String car_make=val.findPath("car_make").textValue();
		String car_model=val.findPath("car_model").textValue();
		int car_year=val.findPath("car_year").asInt();
		String car_color=val.findPath("car_color").textValue();

		//Save Car details
		Car car = new Car();
		car.registration_no = car_reg_no;
		car.make = car_make;
		car.model = car_model;
		car.year = car_year;
		car.color = car_color;
		car.save();

		result.put("status",SUCCESS); //0-> Success    1->Failure
		result.put("message","Car info Added Successfully"); 

		return ok(result);
	}

}

