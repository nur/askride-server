/**
 * Copyright 2012 Jorge Aliss (jaliss at gmail dot com) - twitter: @jaliss
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package controllers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import manager.LoginManager;
import models.Passenger;
import models.PassengersPayment;
import models.User;
import models.UserLocation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
//import securesocial.core.Identity;
//import securesocial.core.java.SecureSocial;
import securesocial.core.BasicProfile;
import securesocial.core.java.SecureSocial;
import securesocial.core.java.SecuredAction;
import views.html.*;

//For stripe
import java.util.HashMap;
import java.util.Map;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;



/**
 * A sample controller
 */
public class Login extends Controller {
	/**
	 * This action only gets called if the user is logged in.
	 *
	 * @return
	 */
	private static final int SUCCESS=0;
	private static final int FAILURE=1;
	private static final char MILE='M';
	private static final double RANGE_LIMIT=5.0;
  private static final char ACTIVE='Y';
  private static final char INACTIVE='N';
  private static final char PASSENGER='P';
  private static final char DRIVER='D';

	public static Result dummy() {		
		return ok("Default Index Page");

	}
    
	//@SecureSocial.SecuredAction
	public static Result index() {
		BasicProfile user = (BasicProfile) ctx().args.get(SecureSocial.USER_KEY);
		return ok("Default Index Page");

	}

	/*    @SecureSocial.UserAwareAction
    public static Result userAware() {
        Identity user = (Identity) ctx().args.get(SecureSocial.USER_KEY);
        final String userName = user != null ? user.fullName() : "guest";
        return ok("Hello " + userName + ", you are seeing a public page");
    }

    @SecureSocial.SecuredAction( authorization = WithProvider.class, params = {"facebook"})
    public static Result onlyTwitter() {

        return ok("You are seeing this because you logged in using Twitter \n First Name : " + SecureSocial.currentUser().firstName() + 
        		                                                          "\nemail: " + SecureSocial.currentUser().email().get() + 
        		                                                          "\nPassWd: " + SecureSocial.currentUser().passwordInfo() + 
        		                                                          "\nKEY: " + SecureSocial.USER_KEY +
        		                                                          "\nUserID: " + SecureSocial.currentUser().identityId().userId() +
        		                                                          "\nProduct Prefix: "+ SecureSocial.currentUser().identityId().productPrefix() + 
        		                                                          "\nAvatar URL:" + SecureSocial.currentUser().avatarUrl() +
        		                                                          "\nProvider name: " + SecureSocial.currentUser().identityId().providerId());
    }*/

	//@SecureSocial.SecuredAction( authorization = WithProvider.class, params = {"facebook"})
	@SecuredAction(authorization = WithProvider.class, params = {"facebook"})
	public static Result userExist() {

		// boolean exists = false;
		ObjectNode result = Json.newObject();
		LoginManager loginMgr = new LoginManager();
		services.User userSvc = (services.User) ctx().args.get(SecureSocial.USER_KEY);
		
		if(loginMgr.userExists(userSvc) == true) {
			result.put("status",SUCCESS); //0-> Success    1->Failure

			result.put("user_name", userSvc.getMain().userId());
			result.put("auth_provider", userSvc.getMain().providerId());
			result.put("first_name", userSvc.getMain().firstName().get());
			result.put("last_name", userSvc.getMain().lastName().get());
			result.put("email_id", userSvc.getMain().email().isDefined()? userSvc.getMain().email().get() : "Not available");
			result.put("token", userSvc.getMain().oAuth2Info().get().accessToken());
			result.put("expires_in", userSvc.getMain().oAuth2Info().get().expiresIn().toString());
			
			
//			result.put("user_name", user.userId());
//			result.put("auth_provider", user.providerId());
//			result.put("first_name", user.firstName().toString());
//			result.put("last_name", user.lastName().toString());
//			result.put("email_id", user.email().isDefined()? user.email().get() : "Not available");
//			result.put("token", user.oAuth2Info().get().accessToken());
//			result.put("expires_in", user.oAuth2Info().get().expiresIn().toString());
		}
		else
		{
			result.put("status",FAILURE); //0-> Success    1->Failure
			result.put("message","Error calling userExists()"); //0-> Success    1->Failure
		}
		return ok(result);
	}


	//@SecureSocial.SecuredAction( authorization = WithProvider.class, params = {"facebook"})
	@SecuredAction(authorization = WithProvider.class, params = {"facebook"})
	public static Result findUser(long id) {

		// Find a user by ID
		User user = User.find.byId(id);

		JsonNode result = Json.toJson(user);
		return ok(result);
	}


	//@SecureSocial.SecuredAction( authorization = WithProvider.class, params = {"facebook"})
	public static Result saveCCInfo() {
			/* 
			 * How to call saveCCInfo() using curl
			 * 
			 * curl -X POST -H "Content-Type: application/json" -d " {\"user_name\":\"686023461494044\", \"latitude\":\"42.1698964\", \"longitude\":\"-87.8465724\", \"user_type\":\"D\", \"active_ind\":\"Y\"}" http://localhost:9000/saveUserLocation		 *
			 *
			 */
			JsonNode val = request().body().asJson();
			ObjectNode result = Json.newObject();

			// System.out.println(val);

			String userName=val.findPath("user_name").textValue();
			String cardNumber=val.findPath("card_number").textValue();
			String expDate=val.findPath("exp_date").textValue();
			String cvv=val.findPath("cvv").textValue();
			String personalPin=val.findPath("personal_pin").textValue();
			
			Passenger passenger = new Passenger();

			passenger.user_name = userName;
			passenger.credit_card_no = cardNumber;
			passenger.exp_date = expDate;
			passenger.cvv = cvv;
			passenger.pin = personalPin;
			passenger.total_bonus = (long) 0;
			passenger.total_miles_travelled = (long) 0;
			
			passenger.save();

			result.put("status", SUCCESS); //0-> Success    1->Failure
			result.put("message", "Passenger CC info saved Successfully"); 

			return ok(result);
	}

	//@SecureSocial.SecuredAction( authorization = WithProvider.class, params = {"facebook"})
	public static Result savePaymentDetails() {
			/* 
			 * How to call saveCCInfo() using curl
			 * 
			 * curl -X POST -H "Content-Type: application/json" -d " {\"user_name\":\"686023461494044\", \"latitude\":\"42.1698964\", \"longitude\":\"-87.8465724\", \"user_type\":\"D\", \"active_ind\":\"Y\"}" http://localhost:9000/saveUserLocation		 *
			 *
			 */
			JsonNode val = request().body().asJson();
			ObjectNode result = Json.newObject();

			// System.out.println(val);

			String userName=val.findPath("user_name").textValue();
			Long transactionId=val.findPath("transaction_id").longValue();
			float fareAmount=val.findPath("fare_amount").floatValue();
			
			PassengersPayment passengerPayment = new PassengersPayment();

			passengerPayment.user_name = userName;
			passengerPayment.transaction_id = transactionId;
			passengerPayment.ride_id = (long) 1;
			passengerPayment.fare_amount = fareAmount;
			passengerPayment.discount_offer = 0;
			
			DateFormat df = new SimpleDateFormat("MM/DD/YYYY HH:mm:ss");
			Date dateobj = new Date();
			passengerPayment.create_date_time = df.format(dateobj);
						
			passengerPayment.save();

			result.put("status", SUCCESS); //0-> Success    1->Failure
			result.put("message", "Passenger Transaction details saved Successfully"); 

			return ok(result);
	}

	public static Result doCCTransaction() {
		/* 
		 * How to call saveCCInfo() using curl
		 * 
		 * curl -X POST -H "Content-Type: application/json" -d " {\"user_name\":\"686023461494044\", \"latitude\":\"42.1698964\", \"longitude\":\"-87.8465724\", \"user_type\":\"D\", \"active_ind\":\"Y\"}" http://localhost:9000/saveUserLocation		 *
		 *
		 */
		JsonNode val = request().body().asJson();
		ObjectNode result = Json.newObject();

		// System.out.println(val);

		String userName=val.findPath("user_name").textValue();
		String token=val.findPath("token").textValue();
		String amount=val.findPath("amount").textValue();
		
		
		
		
		Stripe.apiKey = "YOUR-SECRET-KEY";
        Map<String, Object> chargeMap = new HashMap<String, Object>();
        
        Map<String, Object> cardMap = new HashMap<String, Object>();
        cardMap.put("number", "4242424242424242");
        cardMap.put("exp_month", 12);
        cardMap.put("exp_year", 2020);
        
        chargeMap.put("amount", 100);
        chargeMap.put("currency", "usd");
        chargeMap.put("card", cardMap);
        
        try {
            Charge charge = Charge.create(chargeMap);
            System.out.println(charge);
    		result.put("status", SUCCESS); //0-> Success    1->Failure
    		result.put("message", "A Transaction of " + amount + " has been performed SUCCESSFULLY for User: " + userName); 
           
        } catch (StripeException e) {
            e.printStackTrace();
    		result.put("status", FAILURE); //0-> Success    1->Failure
    		result.put("message", "Transaction FAILED of " + amount + " for User: " + userName); 

        }
        
     	return ok(result);
}

	//@SecureSocial.SecuredAction( authorization = WithProvider.class, params = {"facebook"})
	public static Result saveUserLocation() {
		/* 
		 * How to call createUser() using curl
		 * 
		 * Driver1 : curl -X POST -H "Content-Type: application/json" -d " {\"user_name\":\"686023461494044\", \"latitude\":\"42.1698964\", \"longitude\":\"-87.8465724\", \"user_type\":\"D\", \"active_ind\":\"Y\"}" http://localhost:9000/saveUserLocation		 *
		 * Driver2 : curl -X POST -H "Content-Type: application/json" -d " {\"user_name\":\"686023461494045\", \"latitude\":\"42.1672961\", \"longitude\":\"-87.8431821\", \"user_type\":\"D\", \"active_ind\":\"Y\"}" http://localhost:9000/saveUserLocation
		 * Driver4 : curl -X POST -H "Content-Type: application/json" -d " {\"user_name\":\"686023461494046\", \"latitude\":\"42.03358\", \"longitude\":\"-87.7454853\", \"user_type\":\"D\", \"active_ind\":\"Y\"}" http://localhost:9000/saveUserLocation
		 * Driver3 : curl -X POST -H "Content-Type: application/json" -d " {\"user_name\":\"686023461494047\", \"latitude\":\"42.1673279\", \"longitude\":\"-87.8497803\", \"user_type\":\"D\", \"active_ind\":\"N\"}" http://localhost:9000/saveUserLocation
		 *
		 */
		JsonNode val = request().body().asJson();
		ObjectNode result = Json.newObject();

		// System.out.println(val);

		String user_name=val.findPath("user_name").textValue();
		String latitude=val.findPath("latitude").textValue();
		String longitude=val.findPath("longitude").textValue();
		char user_type=val.findPath("user_type").textValue().charAt(0);
		char active_ind=val.findPath("active_ind").asText().charAt(0);


		UserLocation userLoc = new UserLocation();

		userLoc.user_name = user_name;
		userLoc.longitude = longitude;
		userLoc.latitude = latitude;
		userLoc.user_type = user_type;
		userLoc.active_ind = active_ind;

		userLoc.save();

		result.put("status",SUCCESS); //0-> Success    1->Failure
		result.put("message","Location Added Successfully"); 

		return ok(result);
	}

	//@SecureSocial.SecuredAction( authorization = WithProvider.class, params = {"facebook"})
	public static Result findAllDriverNearMe() {
		/*
		 * How to call : curl -X POST -H "Content-Type: application/json" -d " {\"latitude\":\"42.1673279\", \"longitude\":\"-87.845757\"}" http://localhost:9000/findAllDriverNearMe
		 * 
		 * Data Setup:
		 * -----------
		 * Passenger From Location - "42.1673279\", \"-87.845757\"

		 * Driver1 - user_name:686023461494044 - Y: 42.1698964, -87.8465724
		 * Driver2 - user_name:686023461494045 - Y: 42.1672961, -87.8431821
		 * Driver4 - user_name:686023461494046 - Y: 42.03358, -87.7454853 ---far away
		 * Driver3 - user_name:686023461494047 - N: 42.1673279, -87.8497803 
		 */

		JsonNode request = request().body().asJson();

		double fromLatitude  = request.findPath("latitude").asDouble();
		double fromLongitude = request.findPath("longitude").asDouble();

		// Find all Users
		List<UserLocation> usersLocation = UserLocation.find.all();
		List<UserLocation> usersLocationNearMe = new ArrayList<UserLocation>();
		UserLocation myLocation = new UserLocation();
		
		double distanceInMile = 0.0;
		
		System.out.println("fromLatitude = " + fromLatitude);
		System.out.println("fromLongitude = " + fromLongitude);
        
		//Set my location (passenger location) as first location in the list of user location
		
		// myLocation.user_name = SecureSocial.currentUser().identityId().userId() //uncomment when TEST is over
		myLocation.user_name = "00000";
		myLocation.latitude = request.findPath("latitude").toString();
		myLocation.longitude = request.findPath("longitude").toString();
		myLocation.user_type = PASSENGER;
		myLocation.active_ind = ACTIVE;
		usersLocationNearMe.add(myLocation);
		
		for (UserLocation userLoc : usersLocation) {
			double toLatitude =  Double.parseDouble(userLoc.latitude);
			double toLongitude = Double.parseDouble(userLoc.longitude);	
			char user_type = userLoc.user_type;
			char active_ind = userLoc.active_ind;
			System.out.println("-----------------------------------------------------");
			System.out.println("toLatitude = " + toLatitude);
			System.out.println("toLongitude = " + toLongitude);
			System.out.println("active_ind =" + active_ind);
            
			/*
			 *  Get distance between passenger and the driver location. If it is within 5 mile 
			 *  then return this Driver location to client 
			 */
			if(active_ind == ACTIVE && user_type == DRIVER) {
				distanceInMile = getDistance(fromLatitude, fromLongitude, toLatitude, toLongitude, MILE);
				System.out.println("distanceInMile = " + distanceInMile);
				
				if (distanceInMile <= RANGE_LIMIT) {
					usersLocationNearMe.add(userLoc);
				}				
			}
			
		}

		JsonNode allDriverLocationNearMe = Json.toJson(usersLocationNearMe);

		ObjectNode result = Json.newObject();

		result.put("status", SUCCESS); //0-> Success    1->Failure
		result.put("allDriverLocationNearMe", allDriverLocationNearMe);

		return ok(result); 
	}

	/* Commented as it is not giving correct distance
    double DirectDistance(double lat1, double lng1, double lat2, double lng2) 
    {
      double earthRadius = 3958.75;
      double dLat = Math.toRadians(lat2-lat1);
      double dLng = Math.toRadians(lng2-lng1);
      double a = Math.sin(dLat/2) * Math.sin(dLat/2) + 
    		     Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * 
    		     Math.sin(dLng/2) * Math.sin(dLng/2);
      double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
      double dist = earthRadius * c;
      double meterConversion = 1609.00;
      return dist * meterConversion;
    }
	 */

	/* 
	 * I tested this function and compared with wikimapia distance. It is giving almost same value but not 100% accurate.
	 * I tested the implementation [above DirectDistance()] of Haversine formula from - http://www.codecodex.com/wiki/Calculate_Distance_Between_Two_Points_on_a_Globe
	 * But it is not giving the distance properly.
	 */
	public static final double getDistance(double lat1, double lon1, double lat2, double lon2, char unit)
	{
		System.out.println("lat1 = " + lat1 + "     lon1 = " + lon1);
		System.out.println("lat2 = " + lat2 + "     lon2 = " + lon2);
		
	    double theta = lon1 - lon2;
	    double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
	    dist = Math.acos(dist);
	    dist = rad2deg(dist);
	    dist = dist * 60 * 1.1515;
	     
	    if (unit == 'K') {
	        dist = dist * 1.609344;
	    }
	    else if (unit == 'N') {
	        dist = dist * 0.8684;
	    }
	    
	    System.out.println("dist = " + dist);
	    return (dist);
	}
	 
	/**
	 * <p>This function converts decimal degrees to radians.</p>
	 * 
	 * @param deg - the decimal to convert to radians
	 * @return the decimal converted to radians
	 */
	private static final double deg2rad(double deg)
	{
	    return (deg * Math.PI / 180.0);
	}
	 
	/**
	 * <p>This function converts radians to decimal degrees.</p>
	 * 
	 * @param rad - the radian to convert
	 * @return the radian converted to decimal degrees
	 */
	private static final double rad2deg(double rad)
	{
	    return (rad * 180 / Math.PI);
	}

}
