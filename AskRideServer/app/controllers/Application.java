package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import securesocial.core.BasicProfile;
import securesocial.core.java.SecureSocial;
import securesocial.core.java.SecuredAction;
import services.User;

public class Application extends Controller {

	public static Result index() {
		BasicProfile user = (BasicProfile) ctx().args.get(SecureSocial.USER_KEY);
		return ok(views.html.index.render(user != null));
	}

	@SecuredAction(authorization = WithProvider.class, params = {"facebook"})
	public static Result start() {
		User user = (User) ctx().args.get(SecureSocial.USER_KEY);
		String msg = "Hello " + user.getMain().fullName().get() + " from " + user.getMain().providerId() + "!" 
		+ "\n USER_KEY = " + user.getMain().oAuth2Info()  + "\n Basic Profile = " + user.identities  + "\n\n\n oAuth2Info = " + user.getMain().oAuth2Info()
		+ "\n\n\n Token = " + user.getMain().oAuth2Info().get().accessToken() + "\n\n\n Expires IN = " + user.getMain().oAuth2Info().get().expiresIn() ;
		return ok(views.html.start.render(msg));
	}

}
