package manager;

import models.User;
import securesocial.core.BasicProfile;
import securesocial.core.java.SecureSocial;

public class LoginManager {
	public boolean userExists(services.User user) {
		if(findUserByEmailId(user.getMain().email().get()) == true) {

			//Do nothing
		}
		else
		{
			createUser(user);
		}
		return true;
	}
    
    private boolean findUserByEmailId(String email) {
    	
    	User user = User.usersByEmail(email);
    	if(user != null) {
    		return true;
    	}
    	return false;
    }
    
    private boolean createUser(services.User userBasicProfile) {
    	
        User user = new User();
        
        user.user_name = userBasicProfile.getMain().userId();
        user.auth_provider = userBasicProfile.getMain().providerId();
        user.password = userBasicProfile.getMain().passwordInfo().toString();
        user.first_name = userBasicProfile.getMain().firstName().get();
        user.last_name = userBasicProfile.getMain().lastName().get();
        user.email_id = userBasicProfile.getMain().email().isDefined()? userBasicProfile.getMain().email().get() : "Not available";
        user.phoneno = "0000000000";
        
        user.save();
        
        return true;
   }
}
