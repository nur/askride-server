package manager;

import play.mvc.*;
import play.libs.*;
import play.libs.F.*;

import scala.concurrent.Await;
import scala.concurrent.duration.Duration;
import akka.actor.*;
import static akka.pattern.Patterns.ask;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;



import java.util.*;

import models.UserLocation;

import static java.util.concurrent.TimeUnit.*;

/**
 * A chat room is an Actor.
 */
public class ChatRoom extends UntypedActor {

	//Request type
	private static final String        RIDEREQ = "R";
	private static final String         ACCEPT = "A";
	private static final String   NOTAVAILABLE = "N";
	private static final String   CONFIRMATION = "C";
	private static final String RELEASE_DRIVER = "Q"; //Quit

	//Driver status
	private static final char           ACTIVE = 'Y';
	private static final char         INACTIVE = 'N';

	//User Type
	private static final char        PASSENGER = 'P';
	private static final char           DRIVER = 'D';

	// Default room.
	static ActorRef defaultRoom = Akka.system().actorOf(Props.create(ChatRoom.class));

	// Create a Robot, just for fun.
	static {
		new Robot(defaultRoom);
	}

	/**
	 * Join the default room.
	 */
	public static void join(final String username, WebSocket.In<JsonNode> in, WebSocket.Out<JsonNode> out) throws Exception{

		// Send the Join message to the room
		String result = (String)Await.result(ask(defaultRoom,new Join(username, out), 1000), Duration.create(1, SECONDS));

		if("OK".equals(result)) {

			// For each event received on the socket,
			in.onMessage(new Callback<JsonNode>() {
				public void invoke(JsonNode event) {

					// Send a Talk message to the room. 
					// We are sending the InterestedDriverName as "text" and  rideReq as "reqtype"
					defaultRoom.tell(new Talk(username, event.get("text").asText(), event.get("reqtype").asText()), null);

					//Send Ride request to the particular Driver based on requesting Driver
					//notifyDriver(String rideReq, String RequestingUser, String driver)
					//notifyDriver("rideReq", username, event.get("driver").asText());
				} 
			});

			// When the socket is closed.
			in.onClose(new Callback0() {
				public void invoke() {

					// Send a Quit message to the room.
					defaultRoom.tell(new Quit(username), null);

				}
			});

		} else {

			// Cannot connect, create a Json error.
			ObjectNode error = Json.newObject();
			error.put("error", result);

			// Send the error to the socket.
			out.write(error);

		}

	}

	// Members of this room.
	Map<String, WebSocket.Out<JsonNode>> members = new HashMap<String, WebSocket.Out<JsonNode>>();

	public void onReceive(Object message) throws Exception {

		if(message instanceof Join) {

			// Received a Join message
			Join join = (Join)message;

			// Check if this username is free.
			if(members.containsKey(join.username)) {
				getSender().tell("This username is already used", getSelf());
			} else {
				members.put(join.username, join.channel);
				// notifyAll("join", join.username, "has entered the room"); Palak Commented as he does not want to broadcast Talk messaage to all the connected client
				getSender().tell("OK", getSelf());
			}

		} else if(message instanceof Talk)  {

			// Received a Talk message
			Talk talk = (Talk)message;

			System.out.println("PALAK ==> This is a Talk message talk.username = " + talk.username + " talk.text = " + talk.text);

			//notifyAll("talk", talk.username, talk.text); Palak Commented as he does not want to broadcast Talk messaage to all the connected client

			//Notify Driver/Passenger when msg received
			//notifyDriver("rideReq", talk.username, talk.driver);
			notifyDriver(talk.reqtype, talk.username, talk.text);

		} else if(message instanceof Quit)  {

			// Received a Quit message
			Quit quit = (Quit)message;

			members.remove(quit.username);

			notifyAll("quit", quit.username, "has left the room");

		} else {
			unhandled(message);
		}

	}


	public void notifyDriver(String req, String RequestingUser, String RequestedUser) {

		ObjectNode event = Json.newObject();

		UserLocation userDriver = UserLocation.findAllUserLocationByUserName(RequestedUser);

		System.out.println("PALAK ==> notifyDriver() called. req type = " + req);
		//If the Driver is available
		if(req.equalsIgnoreCase(RIDEREQ) && userDriver.active_ind == ACTIVE && userDriver.user_type == DRIVER) {
			event.put("user", RequestingUser);
			event.put("kind", RIDEREQ);
			event.put("message", RequestingUser + " is requesting for ride to " + RequestedUser);

			System.out.println("RIDEREQ");

			//for(WebSocket.Out<JsonNode> channel: members.values()) {
		}
		//When Driver accepts Passenger's request, send accept message to Passenger
		else if(req.equalsIgnoreCase(ACCEPT)) {

			System.out.println("ACCEPT");
			event.put("user", RequestingUser);
			event.put("kind", ACCEPT);
			event.put("message", RequestingUser + " has accepted " + RequestedUser + "'s request");

		}
		//When Driver accepts Passenger's request, Passenger needs to send CONFIRMATION message to Driver
		else if(req.equalsIgnoreCase(CONFIRMATION)) {

			//Make the Driver Unavailable/INACTIVE in UserLocation table and save it
			UserLocation confirmedDriver = UserLocation.findAllUserLocationByUserName(RequestedUser);
			confirmedDriver.active_ind = INACTIVE;
			confirmedDriver.save();

			System.out.println("CONFIRMATION");
			event.put("user", RequestingUser);
			event.put("kind", CONFIRMATION);
			event.put("message", "Passenger: " + RequestingUser + " has confirmed Driver: " + RequestedUser + ". Driver is going to pick up Passenger");
		}
		//Driver needs to RELEASE when ride is complete
		else if(req.equalsIgnoreCase(RELEASE_DRIVER)) {

			//Make the Driver Unavailable/INACTIVE in UserLocation table and save it
			UserLocation confirmedDriver = UserLocation.findAllUserLocationByUserName(RequestedUser);
			confirmedDriver.active_ind = ACTIVE;
			confirmedDriver.save();

			System.out.println("RELEASE_DRIVER");
			event.put("user", RequestingUser);
			event.put("kind", RELEASE_DRIVER);
			event.put("message", "Passenger: " + RequestingUser + " is releasing Driver: " + RequestedUser + ". Now Driver is available for other ride");
		}		
		else {

			//Send a message to the Requesting Passenger that the DRIVER is not available NOW. Request another DRIVER
			System.out.println("NOTAVAILABLE");
			event.put("user", RequestingUser);
			event.put("kind", NOTAVAILABLE);
			event.put("message", RequestedUser + " is not available for " + RequestingUser);

			//As driver is not available, then notify this fact to Passenger. You do not have to notify anything to Driver, as he/she is busy in riding other Passenger.
			RequestedUser = RequestingUser;
			System.out.println("Now we should notify this fact to Requesting user : " + RequestedUser);
		}

		for(Map.Entry<String, WebSocket.Out<JsonNode>> entry : members.entrySet()) {

			System.out.println("PALAK ==> notifyDriver() - within channel for loop -- " + entry.getKey()); 
			if(entry.getKey().contentEquals(RequestedUser)) {
				System.out.println("PALAK ==> Notified " + RequestedUser);
				entry.getValue().write(event);
			}

		}
	} 

	// Send a Json event to all members
	public void notifyAll(String kind, String user, String text) {
		for(WebSocket.Out<JsonNode> channel: members.values()) {

			ObjectNode event = Json.newObject();
			event.put("kind", kind);
			event.put("user", user);
			event.put("message", text);

			ArrayNode m = event.putArray("members");
			for(String u: members.keySet()) {
				m.add(u);
			}

			channel.write(event);

		}
	}

	// -- Messages

	public static class Join {

		final String username;
		final WebSocket.Out<JsonNode> channel;

		public Join(String username, WebSocket.Out<JsonNode> channel) {
			this.username = username;
			this.channel = channel;
		}

	}

	public static class Talk {

		final String username;
		final String text;
		final String reqtype;

		public Talk(String username, String text, String reqtype) {
			this.username = username;
			this.text = text;
			this.reqtype = reqtype;
		}

	}

	public static class Quit {

		final String username;

		public Quit(String username) {
			this.username = username;
		}

	}

}
