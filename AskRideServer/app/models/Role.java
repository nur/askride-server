package models;

import javax.persistence.*;

import play.db.ebean.*;

@Entity 
@Table(name="role")
public class Role extends Model {

  @Id
  @Column(name="code")
  public String code;
  

  public String user_name;
  
  public String description;
     
  public static Finder<Long,Role> find = new Finder<Long,Role>(Long.class, Role.class); 

}
