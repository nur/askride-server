package models;

import java.util.Date;

import java.sql.Timestamp;

import javax.persistence.*;

import play.db.ebean.*;

@Entity 
@SequenceGenerator(name="DriverSeq", sequenceName="driver_id_seq")
@Table(name="driver")
public class Driver extends Model {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DriverSeq")
  @Column(name="driver_id", unique = true, nullable = false)
  public Long driver_id;
  
  public String user_name;
  public String dl_no;
  
  public String dl_state;
  
  @Column(name="dl_issue_date")
  public Date dl_issue_date;
  
  @Column(name="dl_expiration_date")
  public Date dl_expiration_date;
  
  public String dmv_response;
  public Long rating_1;
  public Long rating_2;
  public Long rating_3;  
  public Long rating_4;  
  public Long rating_5;  
  public Double total_earning;
  @Column(name="last_payment_received_dttm")
  public Timestamp last_payment_received_dttm;
    
  public static Finder<Long,Driver> find = new Finder<Long,Driver>(Long.class, Driver.class); 

}
