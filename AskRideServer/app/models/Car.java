package models;

import javax.persistence.*;

import play.db.ebean.*;

@Entity 
@SequenceGenerator(name="CarSeq", sequenceName="car_id_seq")
@Table(name="car")
public class Car extends Model {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CarSeq")
  @Column(name="car_id", unique = true, nullable = false)
  public Long car_id;
  public Long driver_id;
  public String registration_no;
  public String make;
  public String model;
  public Integer year;
  public String color;  
  public byte [] car_image;
  
    
  public static Finder<Long,Car> find = new Finder<Long,Car>(Long.class, Car.class); 

}
