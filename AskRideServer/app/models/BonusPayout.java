package models;


import javax.persistence.*;

import play.db.ebean.*;

@Entity 
@Table(name="bonus_payout")
public class BonusPayout extends Model {

  @Id
  @Column(name="bonus_id")
  public Long bonus_id;
  public Long ride_id;
  public Long passenger_id;
  public Long bonus_utilized_amount;
   
  public static Finder<Long,BonusPayout> find = new Finder<Long,BonusPayout>(Long.class, BonusPayout.class); 

}
